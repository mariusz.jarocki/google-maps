var app = angular.module('app', [ 'ngMap' ])

app.controller('Ctrl', [ 'NgMap', function(NgMap) {

	var ctrl = this
	ctrl.map = null
	ctrl.selected = null

	NgMap.getMap().then(function(map) {
		
		ctrl.map = map
		var n = Object.keys(map.markers).length
		console.log('Map has been read with', n, 'markers')
		
		for(var m in ctrl.map.markers) {
			ctrl.map.markers[m].id = m
		}

		if(n > 0) {
			var n = 0
			ctrl.selected = Object.values(ctrl.map.markers)[n]
			ctrl.select(n)
		}
	})
	
	var previousMarker = null

	ctrl.click = function() {
		ctrl.selected = this
		ctrl.goTo(this)
	}

	ctrl.select = function(index) {
		ctrl.goTo(ctrl.map.markers[index])
	}

	ctrl.goTo = function(marker) {
		if(previousMarker) previousMarker.setAnimation(null)
		previousMarker = marker
		marker.setAnimation(google.maps.Animation.BOUNCE)
	}
	
}])